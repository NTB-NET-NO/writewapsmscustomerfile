Imports System.Configuration.ConfigurationSettings
Imports System.IO
Imports System.Data.SqlClient
Imports System.Text

Module Module1

    Sub Main()
        MakeCustomerFile()
    End Sub

    Sub MakeCustomerFile()
        '*** Lager fullstendig Customer fil:
        Dim intRowNum As Integer
        Dim strWapFile As String = AppSettings("WAP_CustomerFile")
        Dim strTmpFile As String = AppSettings("WAP_CustomerTempFile")

        Dim strLogPath As String = AppSettings("errFilePath")
        Dim strConnectionString = AppSettings("wap_Kunder_ConnectionString")

        Dim strNTBStoffgrupper As String
        Dim strNTBOmraader As String
        Dim strNTBFylker As String
        Dim strNTBHovedKategorier As String
        Dim strNTBUnderKatSport As String
        Dim strNTBUnderKatOkonomi As String
        Dim strNTBUnderKatKuriosa As String
        Dim strNTBUnderKatKultur As String
        Dim strPrio13Stoffgr As String

        Dim strSlutt As String
        Dim strFullName As String
        Dim strMobilphone As String
        Dim strblnSMS As String
        Dim strblnWAP As String
        Dim strblnSniffertxt As String
        Dim strNTBSniffertxt As String

        Dim cn As SqlConnection = New SqlConnection()
        Try
            'cn.CursorLocation = adUseClient
            cn.ConnectionString = strConnectionString
            cn.Open()

#If Not Debug Then
            ' Testing if any rows in Customer table have been updated:
            Dim mySqlCommand As SqlCommand = New SqlCommand("testWapCustomerFile", cn)
            Dim isUpdated As Boolean = mySqlCommand.ExecuteScalar()
            If Not isUpdated Then
                cn.Close()
                cn.Dispose()
                Exit Sub
            End If
#End If

            Dim SelectCmdString As String = "select * from Customer"
            Dim mySqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(SelectCmdString, cn)
            Dim myDataSet As DataSet = New DataSet()
            mySqlDataAdapter.Fill(myDataSet, "Customer")
            cn.Close()
            cn.Dispose()

            Dim w As StreamWriter = New StreamWriter(strTmpFile, False, Encoding.GetEncoding("iso-8859-1")) '  create a Char writer 

            Dim rsCustomer As DataRow
            For Each rsCustomer In myDataSet.Tables("Customer").Rows
                With rsCustomer
                    intRowNum = intRowNum + 1
                    'left(strVerdi, 4) <> "Alle"

                    strSlutt = ">"
                    strFullName = "<Customer=" & .Item("Customer") & strSlutt
                    strMobilphone = "<Mobilphone=" & .Item("Mobilphone") & strSlutt
                    If .Item("SMS") = "True" Then
                        strblnSMS = "<SMS=Sann" & strSlutt
                    Else
                        strblnSMS = "<SMS=Usann" & strSlutt
                    End If

                    If .Item("WAP") = "True" Then
                        strblnWAP = "<WAP=Sann" & strSlutt
                    Else
                        strblnWAP = "<WAP=Usann" & strSlutt
                    End If

                    strPrio13Stoffgr = "<Prio13Stoffgr=" & Trim(.Item("Prio13Stoffgr") & "") & strSlutt
                    If Trim(.Item("NTBSniffertxt") & "") <> "" Then
                        strblnSniffertxt = "<blnSniffertxt=Sann" & strSlutt
                    Else
                        strblnSniffertxt = "<blnSniffertxt=Usann" & strSlutt
                    End If

                    If Right(.Item("NTBSniffertxt") & "", 1) <> ";" And Trim(.Item("NTBSniffertxt") & "") <> "" Then
                        strNTBSniffertxt = "<NTBSniffertxt=" & .Item("NTBSniffertxt") & ";" & strSlutt
                    Else
                        strNTBSniffertxt = "<NTBSniffertxt=" & .Item("NTBSniffertxt") & strSlutt
                    End If

                    strNTBSniffertxt = Replace(strNTBSniffertxt, "; ", ";")
                    strNTBSniffertxt = Replace(strNTBSniffertxt, " ;", ";")

                    strNTBStoffgrupper = "<NTBStoffgrupper=" & .Item("NTBStoffgrupper") & strSlutt
                    strNTBOmraader = "<NTBOmraader=" & .Item("NTBOmraader") & strSlutt
                    strNTBFylker = "<NTBFylker=" & .Item("NTBFylker") & strSlutt
                    strNTBHovedKategorier = "<NTBHovedKategorier=" & .Item("NTBHovedKategorier") & strSlutt
                    strNTBUnderKatSport = "<NTBUnderKatSport=" & .Item("NTBUnderKatSport") & strSlutt
                    strNTBUnderKatOkonomi = "<NTBUnderKatOkonomi=" & .Item("NTBUnderKatOkonomi") & strSlutt
                    strNTBUnderKatKuriosa = "<NTBUnderKatKuriosa=" & .Item("NTBUnderKatKuriosa") & strSlutt
                    strNTBUnderKatKultur = "<NTBUnderKatKultur=" & .Item("NTBUnderKatKultur") & strSlutt

                    strNTBHovedKategorier = Replace(strNTBHovedKategorier, "Kongestoff;", "Kuriosa;")
                    strNTBHovedKategorier = Replace(strNTBHovedKategorier, "Kuriosa;", "Kuriosa2;", 1, 1)
                    strNTBHovedKategorier = Replace(strNTBHovedKategorier, "Kuriosa;", "")
                    strNTBHovedKategorier = Replace(strNTBHovedKategorier, "Kuriosa2;", "Kuriosa;")

                    strNTBOmraader = Replace(strNTBOmraader, "Norge;", "Norge2;", 1, 1)
                    strNTBOmraader = Replace(strNTBOmraader, "Norge;", "")
                    strNTBOmraader = Replace(strNTBOmraader, "Norge2;", "Norge;")

                    strNTBHovedKategorier = Replace(strNTBHovedKategorier, "AlleHov;", "")
                    strNTBOmraader = Replace(strNTBOmraader, "AlleOmr;", "")
                    strNTBFylker = Replace(strNTBFylker, "AlleFyl;", "")
                    strNTBUnderKatKultur = Replace(strNTBUnderKatKultur, "AlleKul;", "")
                    strNTBUnderKatOkonomi = Replace(strNTBUnderKatOkonomi, "AlleOko;", "")
                    strNTBUnderKatSport = Replace(strNTBUnderKatSport, "AlleSpo;", "")

                    w.WriteLine(strFullName & strMobilphone & strblnSMS & strblnWAP & strPrio13Stoffgr & strblnSniffertxt & strNTBSniffertxt & strNTBStoffgrupper & strNTBOmraader & strNTBFylker & strNTBHovedKategorier & strNTBUnderKatSport & strNTBUnderKatOkonomi & strNTBUnderKatKuriosa & strNTBUnderKatKultur)
                End With
            Next
            w.Flush()                              '  update underlying file
            w.Close()                              '  close the writer and underlying file

            ' Copy file to get a fast owerwrite of old file, to prevent WapSms service to read half files.
            File.Copy(strTmpFile, strWapFile, True)
            File.Delete(strTmpFile)

            myDataSet.Dispose()
            mySqlDataAdapter.Dispose()
        Catch e As Exception
            WriteErr(strLogPath, "Feil i MakeCustomerFile()", e)
        End Try
    End Sub

    Public Sub WriteErr(ByRef strLogPath As String, ByRef strMessage As String, ByRef e As Exception)
        Dim strFile As String = strLogPath & "\Error-" & Format(Now, "yyyy-MM-dd") & ".log"

        On Error Resume Next

        Dim w As StreamWriter = New StreamWriter(strFile, True, Encoding.GetEncoding("iso-8859-1")) '  create a Char writer 
        Dim strLine As String
        strLine = "Error: " & Format(Now, "yyyy-MM-dd hh:mm:ss") & vbCrLf
        strLine &= strMessage & vbCrLf
        strLine &= e.Message & vbCrLf
        strLine &= e.StackTrace & vbCrLf
        strLine &= "------------------------------------------------------------"

        'Console.WriteLine(strLine)

        w.WriteLine(strLine)
        w.Flush()                              '  update underlying file
        w.Close()                              '  close the writer and underlying file
    End Sub

End Module
